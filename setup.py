from setuptools import setup, find_packages

setup(
    name='stvhay-testflask',
    version='0.0.0.0',
    description='Minimal flask CI example GitLab',
    packages=find_packages(),
    install_requires=['Flask == 1.1.2'],
)
